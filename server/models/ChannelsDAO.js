
Channels.createChannelForUser = function(username) {

    Channels.insert({
        name: username,
        username: username,

        description: "",
        //stream_name: "", // Como o usuário seta isso?
        //reputation: 0.0, // Falta a lógica para gerar esse dado
        //followers: 0,
        viewers: 0,
        likes: 0, //serao representados por entidade like/dislike
        dislikes: 0,
        level: 0,

        category_name: "",

        default_service: "",
        
        service: {
            twitchtv: "",
            hitbox: ""
        },

        image: {
            profile_photo_url: "",
            card_photo_url: "",
            channel_photo_url: ""
        }
    });

};

/*

Banco local

Channels.insert({
	name: "Other Channel",
	username: "Isaac",

	description: "Essa é a descrição do meu canal. Meu canal é muito bom, com streams muito legais.",
	stream_name: "Counter Strike - Global Offensive",	// Como o usuário seta isso?
	reputation: 99.2, 									// Falta a lógica para gerar esse dado
	followers: 1020,
	viewers: 40230,
	likes: 10903,
	dislikes: 148,
	level: 12,

	category_name: "RPG",
	
	service: {
				twitchtv: "esl_csgo",
				hitbox: "MaxPita" 
			},

	image: {
			profile_photo_url: "",
			card_photo_url: "",
			channel_photo_url: ""

		}
});


Banco Remoto

Channels.insert({
	name: "Other Channel",
	username: "Bruno",

	description: "Essa é a descrição do meu canal. Meu canal é muito bom, com streams muito legais.",
	stream_name: "Counter Strike - Global Offensive",	// Como o usuário seta isso?
	reputation: 99.2, 									// Falta a lógica para gerar esse dado
	followers: 1020,
	viewers: 40230,
	likes: 10903,
	dislikes: 148,
	level: 12,

	tag_name: "RPG",
	
	service: {
				twitchtv: "esl_csgo",
				hitbox: "MaxPita" 
			},

	image: {
			profile_photo_url: "",
			card_photo_url: "",
			channel_photo_url: ""

		}
});

*/