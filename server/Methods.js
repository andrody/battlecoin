/* Puxa profile do usuário logado no iOS
 *
 * @ for iOS 
 */

Meteor.methods({

    olaMundo: function() {
        console.log("Chamou ola mundo");
    },

    /* Espectadores  */

    insertViewer: function(mySessionId, channel_username) {
        Viewer.createViewer(mySessionId, channel_username);
    },

    //updateViewer: function(mySessionId, channel_username) {
    //Viewer.update({sessionId: mySessionId}, {$set: {channel_username: channel_username}});
    //},

    sessionActiveListAdd: function(sessionId) {
        SessoesAtivas.addSession(sessionId);
    },

    /*
     * iOS Methods
     *
     */

    /*
        User methods
    */

    insertUser: function(userAccount) {

        var user = Meteor.users.findOne({
            "profile.username": userAccount.profile.username
        });

        //if(user) {
        //  throw new Meteor.Error(400, 'Error 400: ' + user.profile.username, 'username already exists');
        //} else {
        if (user) {

            //return {status: "username " + user.profile.username};
            throw new Meteor.Error(
                'username already exists'
            );
        }

        /*
        user = Meteor.users.findOne({
            "email": userAccount.email
        });

        if (user) {

            //return {status: "username " + user.profile.username};
            throw new Meteor.Error({
                code: '401: ',
                reason: 'conflict',
                response: 'email already exists'
            });
        }*/

        console.log(userAccount.profile.username);

        if (userAccount) {
            user = Accounts.createUser({
                email: userAccount.email,
                password: userAccount.password,
                profile: userAccount.profile
            });
        } else {
            return false;
        }

        if (user && userAccount.profile && userAccount.profile.username) {

            // Criando canal do usuario
            Channels.createChannelForUser(userAccount.profile.username);
        }

        return user;
    },

    // Retorna informações de usuários
    userProfile: function(userId) {
        return Meteor.user().profile;
    },

    usernameExists: function(username) {
        var user = Meteor.users.findOne({
            "profile.username": username
        });

        //if(user) {
        //  throw new Meteor.Error(400, 'Error 400: ' + user.profile.username, 'username already exists');
        //} else {
        if (user) {

            //return {status: "username " + user.profile.username};
            throw new Meteor.Error({
                code: '400: ',
                reason: 'conflict',
                response: 'username already exists'
            });
        } else {
            return {
                code: '200',
                response: "username can to be used"
            };
        }
        //}
    },

    'saveImage': function(userId, base64Data) {

        var fileDataBuffer = new Buffer(base64Data, 'base64');
        //Do something with fileDataBuffer, like upload to S3 using meteor-knox.

        var newFile = new FS.File();


        newFile.attachData(fileDataBuffer, {
            type: 'image/png'
        }, function(error) {
            if (error) throw error;

            newFile.name('myFile.png');

            if (fileDataBuffer && newFile) {
                console.log("Nome: " + userId);
                //console.log("Data: " + base64Data);

                //console.log(newFile);

                Images.insert(newFile, function(err, fileObj) {
                    console.log("Imagem persistida");
                    if (fileObj) {
                        console.log("Retornando url");

                        var url = fileObj.url({
                            brokenIsFine: true
                        });

                        console.log(url);

                        Channels.update(userId, {
                            $set: {
                                "image.channel_photo_url": url
                            }
                        });

                        return {
                            url: fileObj.url({
                                brokenIsFine: true
                            })
                        };

                    }
                });

            }

        });

        //return {name: "Teste"};

    },
    /* 
        Channels methods
    */

    insertChannel: function(username) {

        Channels.createChannelForUser(username);
    },

    retrieveMyChannel: function() {
        var currentUser = Meteor.user();
        var channel;

        if (currentUser) {
            channel = Channels.findOne({
                username: currentUser.profile.username
            });

            if (!channel) {
                throw new Meteor.Error({
                    code: '404: ',
                    reason: 'channel not logged',
                    response: "este usuario nao tem canal"
                });
            }
        } else {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        }

        return channel;
    },

    channelById: function(channelId) {
        var channel = Channels.findOne(channelId, {
            fields: {
                _id: 1,
                description: 1,
                name: 1,
                username: 1,
                level: 1,
                tagId: 1,
                likes: 1,
                dislikes: 1,
                viewers: 1,
                "image.profile_photo_url": 1,
                "image.channel_photo_url": 1
            }
        });

        return channel;
    },

    channelIdByUsername: function(usr) {

        var username = "";
        var channel = "";

        if (Meteor.user()) {
            username = Meteor.user().profile.username;
            channel = Channels.findOne({
                username: username
            });
        } else {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        }
        //var user = Meteor.users.findOne();

        if (username && channel) {
            return channel._id;
        } else {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'not found',
                response: "this username don't have channel"
            });
        }
    },

    promotedChannels: function(paramArray) {

        return Channels.find({
            category_name: {
                $exists: true,
                $nin: [""]
            }
        }, {
            fields: {
                _id: 1,
                name: 1,
                username: 1,
                "image.profile_photo_url": 1
            }
        }).fetch();
    },

    mostViewersChannels: function(paramArray) {
        return Channels.find({}, {
            fields: {
                _id: 1,
                description: 1,
                name: 1,
                username: 1,
                level: 1,
                tagId: 1,
                service: 1,
                "image.profile_photo_url": 1,
                "image.channel_photo_url": 1
            }
        }).fetch();
    },

    channelsByCategory: function(categoryId) {

        var channelList = Channels.find({
            tagId: categoryId
        }, {
            fields: {
                _id: 1,
                description: 1,
                name: 1,
                username: 1,
                level: 1,
                tagId: 1,
                likes: 1,
                dislikes: 1,
                viewers: 1,
                "image.profile_photo_url": 1,
                "image.channel_photo_url": 1
            }
        }).fetch();
        return channelList;
    },

    channelsByCategoryName: function(name) {

        var channelList = Channels.find({
            category_name: name
        }, {
            fields: {
                _id: 1,
                description: 1,
                name: 1,
                username: 1,
                level: 1,
                tagId: 1,
                likes: 1,
                dislikes: 1,
                viewers: 1,
                "image.profile_photo_url": 1,
                "image.channel_photo_url": 1
            }
        }).fetch();
        return channelList;
    },

    channelsByAllCategory: function() {

        var channelList = Channels.find({}, {
            fields: {
                _id: 1,
                description: 1,
                name: 1,
                username: 1,
                level: 1,
                tagId: 1,
                likes: 1,
                dislikes: 1,
                viewers: 1,
                "image.profile_photo_url": 1,
                "image.channel_photo_url": 1
            }
        }).fetch();
        return channelList;
    },

    updateChannel: function(data) {

        var username = "";
        var channel = "";

        if (Meteor.user()) {
            username = Meteor.user().profile.username;
        } else {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        }

        console.log("Editando para: " + username);
        console.log("Dados: ");
        console.log(data);

        if (username) {
            Channels.update({
                username: username
            }, {
                $set: data

            }, function(err, records) {
                if (err) {
                    console.log("Erro ");
                    console.log(err);
                } else {
                    console.log("records ");
                    console.log(records);
                }
            });
        }

        return data;
    },

    incrementChannelViewers: function(channel_username, inc) {
        console.log("incrementa canal: " + channel_username + " " + inc);
        Channels.update({
            username: channel_username
        }, {
            $inc: {
                viewers: inc
            }
        });
    },

    /*
        Tags / Categories methods
    */

    categoryList: function() {
        return Tags.find({}, {
            fields: {
                _id: 1,
                name: 1
            },
            sort: {
                name: 1
            }
        }).fetch();
    },

    lastSessionLostedUpdate: function(mySession, channelId) {
        LastSessionLosted.update({
            sessionId: mySession
        }, {
            $set: {
                channelId: channelId
            }
        });

    },
    lastSessionLostedInsert: function(mySession, channelId) {
        LastSessionLosted.insert({
            sessionId: mySession,
            channelId: channelId
        });
    },

    /* 
        Like / Dislike 
    */

    likeAChannel: function(channelId) {
        var currentUser = Meteor.userId();

        if (!currentUser) {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        } else {
            var count = Likes.find({
                channelId: channelId
            }).count();

            var like = Likes.findOne({
                userId: currentUser,
                channelId: channelId
            });

            if (like) {
                Likes.remove(like._id);
                Channels.update(channelId, {
                    $inc: {
                        likes: -1
                    }
                });

                return {
                    like: false,
                    count: count
                };
            } else {
                Likes.insert({
                    userId: currentUser,
                    channelId: channelId
                });
                Channels.update(channelId, {
                    $inc: {
                        likes: 1
                    }
                });

                var dislike = Dislikes.remove({
                    userId: currentUser,
                    channelId: channelId
                });
                if (dislike == 1) {
                    Channels.update(channelId, {
                        $inc: {
                            dislikes: -1
                        }
                    });
                }

                return {
                    like: true,
                    count: count
                };
            }
        }

    },

    iLikeThisChannel: function(channelId) {
        var currentUser = Meteor.userId();

        if (!currentUser) {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        } else {
            var like = Likes.findOne({
                userId: currentUser,
                channelId: channelId
            });

            if (like) {
                return {
                    like: true
                };
            } else {
                return {
                    like: false
                };
            }
        }
    },

    dislikeAChannel: function(channelId) {
        var currentUser = Meteor.userId();

        if (!currentUser) {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        } else {
            var dislike = Dislikes.findOne({
                userId: currentUser,
                channelId: channelId
            });

            if (dislike) {
                Dislikes.remove(dislike._id);
                Channels.update(channelId, {
                    $inc: {
                        dislikes: -1
                    }
                });

                return {
                    dislike: false
                };
            } else {
                Dislikes.insert({
                    userId: currentUser,
                    channelId: channelId
                });
                Channels.update(channelId, {
                    $inc: {
                        dislikes: 1
                    }
                });

                var like = Likes.remove({
                    userId: currentUser,
                    channelId: channelId
                });
                if (like == 1) {
                    Channels.update(channelId, {
                        $inc: {
                            likes: -1
                        }
                    });
                }

                return {
                    dislike: true
                };
            }
        }

    },

    iDislikeThisChannel: function(channelId) {
        var currentUser = Meteor.userId();

        if (!currentUser) {
            throw new Meteor.Error({
                code: '404: ',
                reason: 'user not logged',
                response: "usuario nao esta logado"
            });
        } else {
            var dislike = Dislikes.findOne({
                userId: currentUser,
                channelId: channelId
            });

            if (dislike) {
                return {
                    dislike: true
                };
            } else {
                return {
                    dislike: false
                };
            }
        }
    },

sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  }

});
