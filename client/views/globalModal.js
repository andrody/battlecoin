Template.globalModal.rendered = function() {

    // Desativa escuta por keyup no modal
    Session.set("activeKeyListnnerModal", false);

    // Modal de play-bet não aparece inicialmente
    Session.set('presentGlobalModal', false);

};

Template.globalModal.onDestroyed = function() {

    // Desativa escuta por keyup no modal
    Session.set("activeKeyListnnerModal", false);

    // Modal de play-bet não aparece quando libea o template
    Session.set('presentGlobalModal', false);
};

// Dados que vão ser passados para algum modal
var dataContext = null;

Template.globalModal.helpers({
    /*
     * Booleado que controla apresentação de modal na tela
     * Permite modal aparecer se 'presentChannelPopup' for true
     * e a rota atual for a de um canal
     *
     * return @Bool
     */
    presentOptionPopup: function() {
        var present = Session.get('presentGlobalModal'); // Bool

        if (present) {
            return true;
        } else {
            return false;
        }
    },

    // Retorna modal que vai ser apresentado depois que presentChannelPopup for true
    getModal: function() {
        // Retorna string do template a ser exibido como modal
        var modalTemplate = Session.get('globalModalName');

        return modalTemplate;
    },

    // Provê dados para o modal que será chamado
    getDataContext: function() {

        return dataContext;
    },


});

Template.globalModal.events({

    // Despacha modal # REFACTORY
    'click .dismissModal': function(event) {

        console.log("Teste");

        Session.set('globalModalName', '');

        Session.set('presentGlobalModal', false);
    },

});