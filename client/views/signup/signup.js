
/* Cadastro de usuário */

var userAccount;

Template.signUp.rendered = function(){

	// Recupera dados da sessão, se houver, e preenche inputs
	var email = Session.get('email');
	if(email) {
		$('[id="signup-input-email"]').val(email);
	}
	var username = Session.get('username');
	if(username) {
		$('[id="signup-input-username"]').val(username);
	}

	if(Session.get('lastMenuClicked') != 'signUp') {
		fazEfeitoBotao("signUp");
	}
};

Template.signUp.events({

	"click #signup-done": function (e) {
      	var email = $('[name="email"]').val();
      	Session.set('email', email);

      	var username = $('[name="username"]').val();
      	Session.set('username', username);

		userAccount = {
      		email: email,

      		profile: {
      			username: username,
      		},
      		password: $('[name="password"]').val(),
      		passwordVerification: $('[name="passwordRetry"]').val()
    	}

    	// Chama próximo template de dados
    	Session.set('sidebarModal', 'signUp2');
	}

});

/* Signup step 2 */

var fileImage = null;

// Lista inicial de linguagens
var SelectedLanguages = new Mongo.Collection(null);
SelectedLanguages.insert({value: 1, name: "English"});

Template.signUp2.rendered = function(){

	// Recupera dados da sessão, se houver, e preenche inputs
	var currency = Session.get('currency');
	if(currency) {
		$(".currency option[value='" + currency +"']").attr('selected', true);	
	}
	var country = Session.get('country');
	if(country) {
		$(".country-select option[value='" + country +"']").attr('selected', true);	
	}

	fazEfeitoBotao("signUp");
};

Template.signUp2.helpers({
	// Retorna para tela de signUp2 preview de imagem selecionada
	imagePreview: function () {
		var imageId = Session.get("fileImageSavedId");

		if(imageId)
			return Images.findOne(imageId);
		else
			return null;
	},

	// Retorna array de linguagens selecionadas pelo usuário
	selectedLanguages: function() {
		return SelectedLanguages.find();
	}
});

Template.signUp2.events({

	// Upload da imagem de perfil
	'change .myFileInput': function(event, template) {
		var files = event.target.files;

		var file = new FS.File(files[0]);
		

		// Remove imagem usada anteiormente no preview
		var preiousFileId = Session.get("fileImageSavedId")
		if(file && file.name() && preiousFileId){
			Images.remove(preiousFileId);
		}

		if(file && file.name()) {
			file.teste = true;

			console.log("File: " + file.name());
			
			Images.insert(file, function (err, fileObj) {
	        	// Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        	if (fileObj) {
	        		fileImage = fileObj;
	        		console.log("Saved: " + fileObj.name());

	        		// reactive, sinaliza alteraçãode imagem para imagePreview
					Session.set("fileImageSavedId", fileObj._id);

	        	} else {
					console.log("Erro: " + err);
	        	}
	      	});
		}
		
	},

	// Submete criação de conta
	"click #signup-done": function (e) {

		userAccount.profile.currency = $(".currency" ).val();
		userAccount.profile.country = $(".country-select" ).val();

	    var imageProfile = {
	    	_id: "",
	    	url: ""
	    };

    	if(fileImage) {
	    	imageProfile = {
	    		_id: fileImage._id,
	    		url: fileImage.url({brokenIsFine: true})
	    	}
    	}
    	userAccount.profile.image = imageProfile;

    	var languages = SelectedLanguages.find({}, {_id: false}).fetch();

    	// Desnecessário, mas a busca continua trazendo _id no resultado
		userAccount.profile.languages = new Array();
    	for(var i = 0; i < languages.length ; i++){
    		userAccount.profile.languages[i] = {value: languages[i].value, name: languages[i].name};
    	}

    	Meteor.call('insertUser', userAccount, function (err, res) {
		    if(err){

		    } else {
		    	Meteor.loginWithPassword(userAccount.email, userAccount.password);
		    	console.log("Usuario criado com sucesso.");
		        Router.go("/"); // Redirect user if registration succeeds
		        Session.set("fileImage", false);

		    }

    	});

    	/*
		Accounts.createUser({
		    email: userAccount.email,
		    password: userAccount.password,
		    profile: {
		    	username: userAccount.username,
		    	firstname: userAccount.firstname,
		    	lastname: userAccount.lastname,
		    	birthday: userAccount.birthday,
		    	telephone: userAccount.telephone,
		    	country: userAccount.country,
		    	level: 0,
		    	image: userAccount.image
		    }
		}, function(error){
		    if(error){
		    } else {

		    	console.log("Usuario criado com sucesso.");
		        Router.go("/"); // Redirect user if registration succeeds
		        Session.set("fileImage", false);

		        // Criando canal do usuario
		    	Meteor.call('insertChannel', userAccount.username, function (error, result) {

		    	});;

		    }
		});*/

	},

	// Adiciona a linguagem
	'click .add-language-button': function(event) {
		var name = $( ".select-speak-language option:selected" ).text();
		var value = $(".select-speak-language" ).val();

		var count = SelectedLanguages.find().count();

		// Permite até 5 linguagens
		if(count < 5) {
			$(".select-speak-language option[value='" + value + "']").remove();

			SelectedLanguages.insert({value: value, name: name});
			console.log(SelectedLanguages.find());
		} else {
			conslog.log("Limit languages exceeded");
		}

	},

	// Remove linguagem da listagem
	'click .closex': function(event) {
		var name = this.name;
		var value = this.value;

		var count = SelectedLanguages.find().count();

		if(value != 1 && count > 0){
			SelectedLanguages.remove({name: name});
		}
	},

	// Voltar para primeiro passo do Signup
	'click .back-login': function(event) {

		// Salva na sessão campos já atribuidos
		Session.set('currency', $(".currency" ).val());
		Session.set('country', $(".country-select" ).val());

		// Chama primeiro template de signUp
    	Session.set('sidebarModal', 'signUp');	
    }
});





