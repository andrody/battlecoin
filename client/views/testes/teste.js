
//dataJsonG = null;

Template.teste.rendered = function() {

	// Verificando servico onde o canal esta online
	var ch = Channels.findOne();

	DefineActiveService(ch);
};

Template.teste.events({
	'change .myFileInput': function(event, template) {
		var files = event.target.files;

		var file = new FS.File(files[0]);

		Images.insert(file, function (err, fileObj) {
        	// Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
        	if (fileObj) {
				console.log("Arquivo salvo: " + fileObj.name());
				console.log(fileObj.url());
        	} else {
				console.log("Erro: " + err);
        	}
      });

		
	}
});


Template.imageView.helpers({
	images: function () {
	    return Images.find(); // Where Images is an FS.Collection instance
	}
});

/*
Template.stream.helpers({
	concat: function (a, b, c) {
		return a + b + c;
	},
});

Template.stream.events({
	'click .clicavel': function () {
		var teste = new HttpClient("teste");

		console.log("Clicado");

		teste.getTwitchJson("Nightblue3");
	}
});

*/