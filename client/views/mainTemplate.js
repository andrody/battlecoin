

Template.main.helpers({
    facades: function() {

        var facades = new Array();
        var channels;

        var categories = Tags.find().fetch();

        for (var index in categories) {
        	var name = categories[index].name;
        	channels = Channels.find({category_name: name}).fetch();

            facades.push({
                category_name: name,
                channels: channels
            });
        }

        console.log(facades);

        return facades;
    }
});

Template.main.events({
    'click': function() {
        // ...
    }
});
