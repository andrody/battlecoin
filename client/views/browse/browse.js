Template.browse.rendered = function(){
	
	if(Session.get('lastMenuClicked') != 'browse') {
		fazEfeitoBotao("browse");
	}

	var itemTab = Session.get('browseTabContent');

	if (itemTab){
		fazEfeitoTabItem(itemTab);
	}
	else {
		fazEfeitoTabItem('tabfavorites');
		Session.set('browseTabContent', 'tabfavorites');
	}

	//fazEfeitoTabItem(Session.get('browseTabContent'));
};

//Session.set('browseTabContent', 'tabtags');

Template.browseContent.helpers({
   getTemplate: function () {
     return Session.get('browseTabContent');
   },

   getDataContext: function () {
     return { title: 'My Title' };
   }
});

Template.browse.events({
	"click .browse-option": function(event) {

		var option = event.target.getAttribute('name');
		console.log(option);
		console.log(event.target);
		if (option) {
			fazEfeitoTabItem(option);
			Session.set('browseTabContent', option);
		}

	},

	"click .browse-option": function(event) {

		var option = event.target.getAttribute('name');
		console.log(option);
		console.log(event.target);
		if (option) {
			fazEfeitoTabItem(option);
			Session.set('browseTabContent', option);

			if (option == 'tabsearch') {
				$(".browseTabLabel").addClass("hiddenOpacity");
				$(".game-browser-toolbar").addClass("search-active");
				$(".game-browser-toolbar .search-input").focus();

			}
			else{
				$(".browseTabLabel").removeClass("hiddenOpacity");
				$(".game-browser-toolbar").removeClass("search-active");
			}
		}

	},

	// "onmouseover .browse-option": function(event) {
	// 	var option = event.target.getAttribute('name');
	// 	console.log(option);
	// 	console.log(event.target.childNodes[2])
	// 	$(".browseTabLabel." + option).css("opacity", "1");
	// }

});
