
Template.tabevents.helpers({
	// Retorna lista de apostas
	betList: function() {
		var apostas = Apostas.find();
		return Apostas.find();
	},

	// Indica quantos players apostaram nesse evento
	totalBettors: function(bet) {

		var totalBettors = 0;

		for (var i = 0; i < bet.options.length; i++) {
			totalBettors += bet.options[i].bettors.length;
		}

		return totalBettors;
	},

	// Indica quanto de dinheiro está disponível no pot
	totalPot: function(bet) {

		var potSum = 0;

		for (var i = 0; i < bet.options.length; i++) {
			for (var j = 0; j < bet.options[i].bettors.length; j++) {
				potSum += bet.options[i].bettors[j].bet;
			}
		}

		return potSum;
	}
});

Template.tabevents.events({
	'click .sidebarModalOut': function () {
		
		despachaSidebarModal();	
	}
});
