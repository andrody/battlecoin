
Template.optionPopup.rendered = function() {

 	// Ativa escuta por keyup no modal
	$(document).on('keyup', function (e) {
		if (e.keyCode == 27) { // Se clicou no ESC
			// Despacha modal
			Session.set('presentChannelPopup', false);

			// Desativa escuta por keyup no modal
			$(document).off('keyup');
		}

	});
};

Template.optionPopup.onDestroyed = function() {

};

Template.optionPopup.events({
	'click .close-play-modal': function () {

 		// Desativa escuta por keyup no modal
		Session.set("activeKeyListnnerModal", false);

		// Despacha modal de aposta
		Session.set('presentChannelPopup', false);
		console.log("Fecha aposta!");
	},

    'click .turnoffmodal': function(event){
		Session.set("activeKeyListnnerModal", false);

        Session.set('presentChannelPopup', false);
    }
});

