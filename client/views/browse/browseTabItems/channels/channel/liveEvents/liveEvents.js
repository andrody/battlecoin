
Template.liveEvents.helpers({
	betList: function(channelId) {
		//console.log(param);
		return Apostas.find({channelId: channelId});
	},

});

Template.liveEvents.events({

});

Template.bettd.created = function() {
};

Template.bettd.rendered = function() {
};

var totalBet = function(bet){

	var total = 0;

	$.each(bet.options, function(indexOption,option){
	    $.each(option.bettors, function(indexBettor, objBettor){
	  	     total += objBettor.bet;
	    });
	});

	return total;
};

// Indica opção que escolhi nessa aposta
var myAnswerBet = function(bet){

	var userId = Meteor.userId();
	var myAnswerBet = "- - -";

	if(userId){
		$.each(bet.options, function(indexOption,option){
		    $.each(option.bettors, function(indexBettor, objBettor){
				if(userId == objBettor.bettor) {
	    			myAnswerBet = option.description;
					return false;
				}
		    });
		});
	}

	return myAnswerBet;
};

// Indica quantia que apostei
var myBetValue = function(bet){

	var userId = Meteor.userId();
	var myBetValue = " - - -";

	if(userId){
		$.each(bet.options, function(indexOption,option){
		    $.each(option.bettors, function(indexBettor, objBettor){
				if(userId == objBettor.bettor) {
	    			myBetValue = objBettor.bet;
					return false;
				}
		    });
		});
	}

	return myBetValue;
};

// Indica se apostei 
var betAtBet = function(bet){

	var userId = Meteor.userId();
	var betAtBet = false;

	if(userId){
		$.each(bet.options, function(indexOption,option){
		    $.each(option.bettors, function(indexBettor, objBettor){
				if(userId == objBettor.bettor) {
					betAtBet = true;
					return false;
				}
		    });
		});
	}

	return betAtBet;
};

Template.bettd.helpers({
	betList: function(channelId) {

		return Apostas.find({channelId: channelId});
	},

	iBetIt: function(bet) {

		var userId = Meteor.userId();

		return !betAtBet(bet);
	},

	totalPot: function(bet) {

		return totalBet(bet);
	},

	myAnswer: function(bet) {

		return myAnswerBet(bet);
	},

	myBet: function(bet) {

		return myBetValue(bet);

	}

});
