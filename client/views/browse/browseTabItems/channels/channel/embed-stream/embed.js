var dataContext;

var service;

var searchOnlineStream = function() {

    var channel_username = Session.get('channel_username');

    var channel;

    var seachHitbox = function() {
        Meteor.http.get("https://api.hitbox.tv/media/live/" + channel.service.hitbox, function(err, res) {

            if (res) {
            	console.log("Resposta HITBOX");
            	//console.log(res.content.request);

            	if(res.content.indexOf('"media_is_live":"1"') !=-1) {
            		console.log("HITBOX ONLINE!");
                    //Session.set('online_service', 'hitbox');
                    service = 'hitbox';
            	} else {
            		console.log("HITBOX OFFLINE!");
            	}
            }

        });

    }

    var seachTwitchtv = function() {
        Meteor.http.get("https://api.twitch.tv/kraken/streams/" + channel.service.twitchtv, function(err, res) {

            if (res) {

                if (res.data && res.data.stream) {
                    console.log(res.data.stream);
                    //console.log(self.streamUrl + " está online na twitch.")
                    //successCallback(res.data);
            		console.log("TWITCHTV ONLINE!");
                    //Session.set('online_service', 'twitchtv');
                    service = 'twitchtv';
                } else {
                    //console.log(self.streamUrl + " está offline na twitch.")
                    //failCallback(err);
                    console.log("Verifica stream do hitbox");
                    seachHitbox();
                }
            }

        });
    }

    if (channel_username) {
        console.log("buscando stream para: ");

        channel = Channels.findOne({
            username: channel_username
        });

        console.log(channel_username);
        console.log(channel);

        if (channel) {
        	console.log("Iniciando busca por servico");
        	Session.set('online_service', '');
            seachTwitchtv();

            return;
        }
    }

}

Template.embed.created = function() {
    //this.subscribe('channel_route');
};

Template.embed.rendered = function() {
    //console.log("#### EMBED rendered: " + this.data._id);
    var mySessionId = Meteor.connection._lastSessionId;

    //http://www.twitch.tv/nightblue3
    //Viewer.createViewer(mySessionId, Session.get("channel_id"));
    //Meteor.subscribe("bindEnvironment", mySessionId);
    //console.log(this.data);

    //Channels.update(this.data._id, {$inc: {viewers: 1}});
    var channel_username = Session.get("channel_username");
    Meteor.call("incrementChannelViewers", channel_username, 1);

    dataContext = this.data;

};


Template.embed.helpers({

    srcEmbed: function() {

        //var service = Session.get('online_service');

        /*searchOnlineStream(this.data);

        if (service == 'twitchtv') {
            return "http://www.twitch.tv/" + this.service.twitchtv + "/embed"; //activeService.srcEmbed;
        } else if (service == 'hitbox') {
            return "http://www.hitbox.tv/embed/" + this.service.hitbox; //activeService.srcEmbed;
        }*/

        if(this.default_service) {
            if(this.default_service == 'twitchtv') {
                return "http://www.twitch.tv/"+ this.service.twitchtv + "/embed";
            } else if(this.default_service == 'hitbox') {
                return "http://www.hitbox.tv/embed/" + this.service.hitbox;
            }
        }

    },

    hrefEmbed: function() {
        var service = Session.get('activeService');

        if (service == 'azubu') {
            //this.service.azubu
            return "http://www.twitch.tv/" + "teste" + "picoca_lol?tt_medium=live_embed&tt_content=text_link";
        }
    },

    fullscreamStyle: function() {
        var fullscream = Session.get("fullscream");

        if (fullscream == 'total') {
            return "background-color:black;overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px";
        } else if ('partial') {
            return "background-color:black;";
        }
    },

    /*searchStream: function(param) {
        console.log("Passei this: ");
        console.log(param);

        searchOnlineStream(param);

        return true;
    }*/

});

/*
Template.streamServiceContent.helpers({
    // Provê template de serviço online no momento
    getServiceTemplate: function() {

        return 'genericServiceTemplate';
    },
    // Provê dados para template de serviço online no momento
    getServiceTemplateData: function() {
        dataContext = this;

        return dataContext;
    },
});


Template.twitchServiceTemplate.created = function () {
	console.log('this.data');
	console.log(this.data);
};

Template.azubuServiceTemplate.created = function () {
	console.log('this.data');
	console.log(this.data);
};

Template.twitchServiceTemplate.helpers({

	srcEmbed: function() {
		//esl_csgo
		//picoca_lol
		return "http://www.twitch.tv/esl_csgo/embed";
	},

	hrefEmbed: function() {
		return "http://www.twitch.tv/picoca_lol?tt_medium=live_embed&tt_content=text_link";
	}
});

Template.azubuServiceTemplate.helpers({

	srcEmbed: function() {

		return 'http://players.brightcove.net/3361910549001/497f3c86-b138-45f9-80fc-f7f3c9dc1afa_default/index.html?playlistId=ref:x5tvPlaylist';
	},

});

*/
