/*Deps.autorun(function () {

    var incChannel = Session.get("incrementChannelViewer");

    if(incChannel) {
        console.log("Incrementou canal " + incChannel);
        Channels.update(incChannel, {$inc: {viewers: 1}});
    }

});

Deps.autorun(function () {

    var decChannel = Session.get("decrementChannelViewer");

    if(decChannel) {
        console.log("Decrementou canal " + decChannel);
        Channels.update(decChannel, {$inc: {viewers: -1}});
    }

});
*/

Template.channel.rendered = function() {
    var itemTab = Session.get('channelTabContent');

    // Desativa escuta por keyup no modal
    Session.set("activeKeyListnnerModal", false);

    // Modal de play-bet não aparece inicialmente
    Session.set('presentChannelPopup', false);

    // Faz efeito no item tab inicial e o deixa salvo na sessão
    if (itemTab) {
        fazEfeitoTabItem(itemTab);
    } else {
        fazEfeitoTabItem('liveEvents');
        Session.set('channelTabContent', 'liveEvents');
    }

    $(function($) {

        $(".knob").knob({

            'max': 3600,
            change: function(value) {
                //console.log("change : " + value);
            },
            release: function(value) {
                //console.log(this.$.attr('value'));
                //console.log("release : " + value);
            },
            cancel: function() {
                console.log("cancel : ", this);
            },
            /*format : function (value) {
                return value + '%';
            },*/
            draw: function() {

                // "tron" case
                if (this.$.data('skin') == 'tron') {

                    this.cursorExt = 0.3;

                    var a = this.arc(this.cv) // Arc
                        ,
                        pa // Previous arc
                        , r = 1;

                    this.g.lineWidth = this.lineWidth;

                    if (this.o.displayPrevious) {
                        pa = this.arc(this.v);
                        this.g.beginPath();
                        this.g.strokeStyle = this.pColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                        this.g.stroke();
                    }

                    this.g.beginPath();
                    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                    this.g.stroke();

                    this.g.lineWidth = 2;
                    this.g.beginPath();
                    this.g.strokeStyle = this.o.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                    this.g.stroke();

                    return false;
                }
            }
        });

        // Example of infinite knob, iPod click wheel
        var v, up = 0,
            down = 0,
            i = 0,
            $idir = $("div.idir"),
            $ival = $("div.ival"),
            incr = function() {
                i++;
                $idir.show().html("+").fadeOut();
                $ival.html(i);
            },
            decr = function() {
                i--;
                $idir.show().html("-").fadeOut();
                $ival.html(i);
            };
        $("input.infinite").knob({
            min: 0,
            max: 20,
            stopper: false,
            change: function() {
                if (v > this.cv) {
                    if (up) {
                        decr();
                        up = 0;
                    } else {
                        up = 1;
                        down = 0;
                    }
                } else {
                    if (v < this.cv) {
                        if (down) {
                            incr();
                            down = 0;
                        } else {
                            down = 1;
                            up = 0;
                        }
                    }
                }
                v = this.cv;
            }
        });
    });



    setInterval(function() {
        var value = parseInt($(".knob").attr("data-max")) - 1;

        if (value <= 0) value = 3600;

        $(".knob").attr("data-max", value);
        //console.log("valor eh " + value);
        $('.knob')
            .val(value)
            .trigger('change');


        var width = parseInt($(".bet-time").css("width"));
        $('.knob').trigger(
            'configure', {
                "width": width,

            }
        );
    }, 1000);

};

Template.channel.onDestroyed = function() {

    // Desativa escuta por keyup no modal
    Session.set("activeKeyListnnerModal", false);

    // Modal de play-bet não aparece quando libea o template
    Session.set('presentChannelPopup', false);
};

// Dados que vão ser passados para algum modal
var dataContext = null;

Template.channel.created = function() {

    //dataContext = this.data;
    //console.log('Template.channel.created');
    //console.log(this);
    //console.log(this.data);
};

Template.channel.helpers({
    /*
     * Booleado que controla apresentação de modal na tela
     * Permite modal aparecer se 'presentChannelPopup' for true
     * e a rota atual for a de um canal
     *
     * return @Bool
     
    presentOptionPopup: function() {
        var present = Session.get('presentChannelPopup'); // Bool

        if (present && Router.current().route.getName() === 'channel') {
            return true;
        } else {
            return false;
        }
    },

    // Retorna modal que vai ser apresentado depois que presentChannelPopup for true
    getModal: function() {
        // Retorna string do template a ser exibido como modal
        var modalTemplate = Session.get('modalChannel');

        return modalTemplate;
    },

    // Provê dados para o modal que será chamado
    getDataContext: function() {

        return dataContext;
    },*/
    visitors: function() {
        return 0;//currentVisitors.of(Iron.Location.get().path);
    },
    // Provê template de serviço online no momento
    getServiceTemplate: function() {

        return 'webembed';
    },
    // Provê dados para template de serviço online no momento
    getServiceTemplateData: function() {

        // Define que dados do modal serão um canal 
        dataContext = this;

        return dataContext;
    },

});

var count = 0;

Template.channel.events({
    // Faz efeito no item tab da tela de canal e o salva na sessão
    "click .channel-option": function(event) {

        var option = event.target.getAttribute('name');

        if (option) {
            fazEfeitoTabItem(option);
            Session.set('channelTabContent', option);
        }

        // #DEBUG
        DefineActiveService(this);

    },

    // Indica se pode mostrar modal na tela
    'click .play-modal': function() {

        Session.set('globalModalName', 'optionPopup');

        Session.set('presentGlobalModal', true);
    },

    // Abre modal de editar canal
    'click #modifyinfo': function() {
        // Define qual template dinâmico (modal) será exibido
        Session.set('globalModalName', 'modifyInfo');

        // Define que dados do modal serão um canal 
        dataContext = this;

        Session.set('presentGlobalModal', true);
    },

    'click #modifybackground': function() {
        // Define qual template dinâmico (modal) será exibido
        Session.set('globalModalName', 'modifyBackground');

        // Define que dados do modal serão um canal 
        dataContext = this;

        Session.set('presentGlobalModal', true);
    },
    'click #modifybios': function() {
        // Define qual template dinâmico (modal) será exibido
        Session.set('globalModalName', 'modifyBios');

        // Define que dados do modal serão um canal 
        dataContext = this;

        Session.set('presentGlobalModal', true);
    },

    'click #modifylinks': function() {
        // Define qual template dinâmico (modal) será exibido
        Session.set('globalModalName', 'modifyLinks');
        console.log("teste");

        Session.set('presentGlobalModal', true);
    },


    'click #dl-menu': function(event) {

        event.stopPropagation();
        $('.dl-menu').addClass('dl-menuopen dl-menu-toggle').on(this.transEndEventName, function() {
            //$( this ).removeClass( 'dl-menu-toggle' );
        });
    },

    'click': function() {
        $('.dl-menu').removeClass('dl-menuopen');

    }



});

/* Retorna template a ser renderizado no tab container */

Template.channelContent.helpers({
    getTemplate: function() {
        return Session.get('channelTabContent');
    },
});
