Template.sidebar.rendered = function() {

    // Desativa modal no startup do template
    Session.set('presentModal', false);

    // Nenhum template está associado ao modal dessa tela
    Session.set('sidebarModal', '');

};

Template.sidebar.helpers({
    // Flag que permite modal aparecer na tela
    presentModal: function() {
        return Session.get('presentModal');
    },

    // Retorna string com nome do modal a ser apresentado
    getModal: function() {
        return Session.get('sidebarModal');
    },

    // Retorna nickname do usuário logado no momento
    userName: function() {

        var currentUser = Meteor.user();

        if (currentUser) {
            return currentUser.profile.username;
        } else {
            return "Log In";
        }

    }


});

Template.sidebar.events({

    // Exibe modal com template
    "click .menu_option": function(event) {

        console.log(event.target);
        var modalName = event.target.getAttribute('value');
        var currentUser = Meteor.userId();

        if (modalName != "logo") {

            // Se clicou no botao equivalente a login, verificar se esta logado
            if (modalName == 'login' && currentUser) {
                //modalName = 'logged';
                var username = Meteor.user().profile.username;
                Router.go('channel', {
                    username: username
                });
                desfazEfeitoPreencheBotao(Session.get('sidebarModal'));
                dispatchSidebarModal();
                return;
            }

            presentSidebarModal(modalName);

        } else {
            $(".logoItem").removeClass("ahover");
            $(".bigoption").addClass("option-overlay-full");
            $('.bigoption').on({
                mouseout: function() {
                    $('.bigoption').removeClass("option-overlay-full");
                    $(".logoItem").addClass("ahover");

                }
            });
        }

    },

    "click .turnoffmodal": function(event) {

        desfazEfeitoPreencheBotao(Session.get("sidebarModal"));
        Session.set('presentModal', false);
        Session.set('sidebarModal', '');


    }






});

/*
 * Controla apresentação do modal na tela de sidebar
 * sidebarModal contém a string do modal a ser exibido
 * presentModal flag que sinaliza que modal pode ou não aparecer na tela
 *
 */

presentSidebarModal = function(modalName) {
    if (Session.get('sidebarModal') == modalName) {
        desfazEfeitoPreencheBotao(modalName);
        dispatchSidebarModal();
    } else {


        var elemento = "." + modalName + "Item" + " a ";

        console.log(elemento);
        console.log($(elemento + ".option-overlay"));

        $(elemento).removeClass("ahover");
        $(elemento + ".option-overlay").addClass("option-overlay-full");
        $(elemento + ".option-content").removeClass("color_" + modalName);

        Session.set('sidebarModal', modalName);
        Session.set('presentModal', true);
    }
}

dispatchSidebarModal = function() {
    Session.set('presentModal', false);
    Session.set('sidebarModal', '');
}
