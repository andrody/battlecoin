Template.logged.rendered = function(){

	if(Session.get('lastMenuClicked') != 'login') {
		fazEfeitoBotao("login");
	}

	var itemTab = Session.get('loggedTabContent');

	if (itemTab){
		fazEfeitoTabItem(itemTab);
	}
	else {
		fazEfeitoTabItem('yoursStuffs');
		Session.set('loggedTabContent', 'yoursStuffs');
	}
	
};

Template.logged.events({
	"click .user-option": function(event) {

		var option = event.target.getAttribute('name');
		console.log(option);
		console.log(event.target);

		if (option != 'logout') {
			fazEfeitoTabItem(option);
			Session.set('loggedTabContent', option);
		} else {
			Meteor.logout();

			presentSidebarModal('login');
		}

	},

});

Template.loggedTabContent.helpers({
   getTemplate: function () {
     return Session.get('loggedTabContent');
   },

   getDataContext: function () {
     return { title: 'My Title' };
   }
});

