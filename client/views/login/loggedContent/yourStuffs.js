

Template.yoursStuffs.helpers({

	// Exibe imagem do usuário logado
	profilePhoto: function() {

		var currentUser = Meteor.userId();

		if(currentUser) {
			return Meteor.user().profile.image.url;
		} else {
			return "/imgs/yourImg.png";
		}

	}

});