Template.login.rendered = function(){

	if(Session.get('lastMenuClicked') != 'login') {
		fazEfeitoBotao("login");
	}
	
};

Template.login.events({

	// Chama tela de cadastro
	"click #open-account-button": function (event) {

		presentSidebarModal('signUp');
	},

	// Faz autenticação
	"click #login-button-action": function (event) {

      	var email = $('[id="login-input"]').val();
      	var password = $('[id="password-input"]').val();

		var route = "browse";

		console.log("Logando.");

		Meteor.loginWithPassword(email, password, function(error){
		    if(error){
		    	console.log("Erro ao logar.")
		        console.log(error.reason);
		    } else {

		    	console.log("Logado com sucesso.")
		        //presentSidebarModal('logged');
		        dispatchSidebarModal();
		    }
		});

	},

});






