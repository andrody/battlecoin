// Do not forget to add the email package: $ meteor add email
// and to configure the SMTP: https://gist.github.com/LeCoupa/9879221

var processed = false;

Template.ForgotPassword.rendered = function () {
  // ...
  Session.set('forgot-message-text', 'Click Send to reset your password');
  Session.set('forgot-message-color', '#000000');

/*
  Meteor.call('sendEmail',
            'bruno.rocha2008@gmail.com',
            'bruno.rocha2008@gmail.com',
            'Hello from Meteor!',
            'This is a test of Email.send.');*/

  processed = false;
};

var token, done;

// Definindo rota para resetPassword
Accounts.onResetPasswordLink(function (t, d)
{
    token = t;
    done = d;
    setTimeout(()=>Router.go("ResetPassword"), 0);
});

Template.ForgotPassword.events({
  'submit #forgotPasswordForm': function(e, t) {
    e.preventDefault();

    var forgotPasswordForm = $(e.currentTarget);
    var email = forgotPasswordForm.find('#forgotPasswordEmail').val().toLowerCase().trim();

    Session.set('forgot-message-text', 'Processing...');
    console.log(email);


    if(!processed) {

      if (email) {
        Accounts.forgotPassword({email: email}, function(err) {
          console.log('forgotPassword.');

          if (err) {
            if (err.message === 'User not found [403]') {
              console.log('This email does not exist.');
              Session.set('forgot-message-text', 'This email does not exist.');
            } else {
              console.log('We are sorry but something went wrong.');
              Session.set('forgot-message-text', 'We are sorry but something went wrong.');
            }
            Session.set('forgot-message-color', "#FF0000");

          } else {
            console.log('Email Sent. Check your mailbox.');
            Session.set('forgot-message-text', 'Email Sent. Check your mailbox.');
            Session.set('forgot-message-color', "#0000ff");
          }

          processed = true;
        });
      }

    } else {
        Session.set('forgot-message-text', 'Ended process.');
        Session.set('forgot-message-color', "#ff0000");
    }

    return false;
  },
});

Template.ForgotPassword.helpers({
 // texto da mensagem de aviso
 message: function() {
  var message =  Session.get('forgot-message-text');

  return message;
 },

 // cor da mesagem de aviso
 style: function() {
  var color =  Session.get('forgot-message-color');

  return "color: " + color + ";font-size: 2em;width: 80%;transform: translateX(10%);margin: 1em;"; 
 },
});

if (Accounts._resetPasswordToken) {
  Session.set('resetPassword', Accounts._resetPasswordToken);
}

/*

*/

Template.ResetPassword.rendered = function () {
  // ...
  Session.set('reset-message-text', '');

};

Template.ResetPassword.helpers({
 resetPassword: function(){
  return Session.get('resetPassword');
 },

 message: function() {
  var message = Session.get('reset-message-text');

  return message;
 }
});

Template.ResetPassword.events({
  'submit #resetPasswordForm': function(e, t) {
    e.preventDefault();
    
    var resetPasswordForm = $(e.currentTarget),
        password = resetPasswordForm.find('#resetPasswordPassword').val(),
        passwordConfirm = resetPasswordForm.find('#resetPasswordPasswordConfirm').val();

    Session.set('reset-message-text', 'Processing...');

    if (password && (password == passwordConfirm)) { //areValidPasswords
      Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
        if (err) {
          console.log('We are sorry but something went wrong.');
          Session.set('reset-message-text', 'We are sorry but something went wrong.');
        } else {
          console.log('Your password has been changed. Welcome back!');
          Session.set('resetPassword', null);
          Session.set('reset-message-text', 'Your password has been changed. Welcome back!');
          Router.go("/");
        }
      });
    } else {
        Session.set('reset-message-text', 'Please, insert two equals valid passwords.');
    }

    return false;
  }
});