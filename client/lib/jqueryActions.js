
fazEfeitoInput = function (element){

	var getInput = element ;

	$(getInput).focus(function(){
	 	$(this).css("background-color", "white");
	 	//console.log("focado");
	});

	if(getInput == ""){
		$("input").focusout(function(){
		    $(this).css("background-color", "transparent");
		});
	}
	
	if(getInput != ""){
		$("input").focusout(function(){
		    $(this).css("background-color", "white");
		    //console.log("desfocado com texto");
		});
	}
	
}

/*
 * Efeitos dos botoes da SIDEBAR
*/

fazEfeitoBotao = function (element){

	// console.log(element);
	var lastElement = Session.get('lastMenuClicked');

	// var liAnimation = '[name="over-' + element + '"]';
	// var collorEfect =  '[name="color-' + element + '"]';


	// if (Session.get('lastMenuClicked') == element && element == 'browse') {
	// 	$('[name="over-' + lastElement + '"]').animate({width: '2%'},0),
	// 	$('[name="color-' + lastElement + '"]').css("color", Session.get('color'));
	// 	Session.set('lastMenuClicked', '');
	// 	return;
	// } else 
	// // if (Session.get('lastMenuClicked') == element) {
	// // 	return;
	// // }

	desfazEfeitoPreencheBotao(lastElement);

   	efeitoPreencheBotao(element);

	Session.set('lastMenuClicked', element);
}

// Preenche botao com cor lateral
efeitoPreencheBotao = function (element){
	// var liAnimation = '[name="over-' + element + '"]';
	// var collorEfect =  '[name="color-' + element + '"]';

 //   	// $(liAnimation).animate({width: '100%'},100);
 //   	$(liAnimation).animate({width: '100%'},100);
 //   	Session.set('color', $(collorEfect).css("color"));
   	
}

// Despreenche botao com cor lateral
desfazEfeitoPreencheBotao = function (element){

	// $('[name="over-' + element + '"]').animate({width: '2%'},0),
	// $('[name="color-' + element + '"]').css("color", Session.get('color'));
	// Session.set('lastMenuClicked', '');

    var elemento = "." + element + "Item" + " a ";

	$( elemento ).addClass("ahover");
    $( elemento + ".option-overlay" ).removeClass("option-overlay-full");
    $( elemento + ".option-content" ).addClass("color_" + element);

}

// Colore texto dentro da div do botao
efeitoColoreTextoBotao = function(element){
	var collorEfect =  '[name="color-' + element + '"]';

    $(collorEfect).css("color", "#303838");
}

desfazEfeitoColoreTexto = function(element) {

}

/*
 * Faz efeito de deixar item da menu bar selecionado.
*/

fazEfeitoTabItem = function (element, NaoDesfazEfeitoAnteior){

	var targetItem = '[name="' + element + '"]';
	var lastElement = Session.get('lastItemSelected');

	//if (!NaoDesfazEfeitoAnteior) {
		// $('[name="' + lastElement + '"]').animate({height: '0px'},0),
		$('[name="' + lastElement + '"]').removeClass("tab-selected");
	//}

   	// $(targetItem).animate({height: '3px'},0);
   	$(targetItem).addClass("tab-selected");
   	Session.set('lastItemSelected', element);

}

