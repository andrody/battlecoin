/*
	Carrega a lógica para busca por serviço de stream ativo do canal
	que é recebido por parâmetro

	Gera:
		Variável de sessão que carrega o nome do serviço online/ativo com maior prioridade
		'activeService'Nome da variável: 'activeService'
*/


// Guarda servico de stream ativo no momento
activeService = {
	serviceName: "",
	userStream: "",
	priority: 999,

};

DefineActiveService = function(ch) {

	var lastService = activeService;

	if(ch){
		var httpClient = new HttpClientServices(ch);

		/* verifica se esta online na TwitchTv */
		if(ch && ch.service.twitchtv && lastService.serviceName != "twitchtv" && lastService.userStream != ch.service.twitchtv) {
			httpClient.twitchTvIsOnline(
				// executa se canal esta online
				function(dataJson){
			  		console.log(httpClient.channel.name + " está online na twitch.");
			  		switchActiveService("twitchtv", 0);
			}, 
				// executa se canal nao esta online
				function(error){
			  		console.log(httpClient.channel.name + " está offline na twitch.");
			});
		}


		/* verifica se esta online na TwitchTv */
		if(ch && ch.service.azubu  && lastService.serviceName != "azubu" && lastService.userStream != ch.service.azubu) {
			httpClient.azubuIsOnline(
				// executa se canal esta online
				function(dataJson){
					//dataJsonG = dataJson;
			  		console.log(httpClient.channel.name + " está online na azubu.");
			  		switchActiveService("azubu", 1);
			}, 
				// executa se canal nao esta online
				function(error){
			  		console.log(httpClient.channel.name + " está offline na azubu.");
			});
		}

		

	}

	// preenche activeService e define sua prioridade
	var switchActiveService = function(servName, priority) {
		if(activeService) {
			if(priority < activeService.priority){
				activeService.serviceName = servName;
				activeService.priority = priority;
			}
		} else {
			activeService = {
				serviceName: servName,
				priority: priority
			}
		}

		if(activeService.serviceName == 'twitchtv') {
			activeService.srcEmbed = "http://www.twitch.tv/" + ch.service.twitchtv + "/embed";
			activeService.userStream = ch.service.twitchtv;

		} else if(activeService.serviceName == 'azubu') {
			var str = ch.service.azubu;
			var name = str; //.replace("_", "");
			activeService.srcEmbed = "http://players.brightcove.net/3361910549001/497f3c86-b138-45f9-80fc-f7f3c9dc1afa_default/index.html?playlistId=ref:" + ch.service.azubu + "Playlist";
			activeService.userStream = ch.service.azubu;

		}

		//if(lastService != activeService.serviceName) {
			Session.set('activeService', activeService.serviceName);
		//}

		console.log("Servico de stream ativo no momento: " + activeService.serviceName);
		console.log("Prioridade: " + activeService.priority);
		console.log("srcEmbed: " + activeService.srcEmbed);

	};

};
