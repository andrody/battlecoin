Apostas = new Mongo.Collection('apostas');

/*
Banco local: 

Apostas.insert({
	title: "Ferroviario vai ganhar o brasileirão?",
	channel_name: "Other Channel",

	options: [
				{description: "sim", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
				{description: "não", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
				{description: "talvez", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
			 ],

	image: {
			_id: "T9nNEJ5i96Nrf2X4T",
			url: "/cfs/files/images/T9nNEJ5i96Nrf2X4T/event-card-image3.png?token=eyJhdXRoVG9rZW4iOiJ5dE52T0Zhc2o4cVNqSzJoWmYyVzRhLWRGUUN1MGptaWNmZWZMUlJmeVNJIn0%3D"
	}
});

Banco Remoto

Apostas.insert({
	title: "Ferroviario vai ganhar o brasileirão?",
	channel_name: "Other Channel",

	options: [
				{description: "sim", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
				{description: "não", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
				{description: "talvez", bettors: [ {bettor: "id-jose", bet: 12.5}, {bettor: "id-pedro", bet: 10}, {bettor: "id-joao", bet: 5}] },
			 ],

	image: {
				_id: "yMyLsdbZ3nqsony65",
				url: "/cfs/files/images/yMyLsdbZ3nqsony65/event-card-image3.png?token=eyJhdXRoVG9rZW4iOiIifQ%3D%3D"
			}
});

*/