
Accounts.insertUser = function(userAccount) {
    Accounts.createUser({
        email: userAccount.email,
        password: userAccount.password,
        profile: {
            username: userAccount.username,
            firstname: userAccount.firstname,
            lastname: userAccount.lastname,
            birthday: userAccount.birthday,
            telephone: userAccount.telephone,
            country: userAccount.country,
            level: 0,
            image: userAccount.image
        }
    }, function(error) {
        if (error) {
            console.log(error.reason); // Output error if registration fails
        } else {
            console.log("Usuario criado com sucesso.");
            Router.go("/"); // Redirect user if registration succeeds
            Session.set("fileImage", false);
        }
    });

};
