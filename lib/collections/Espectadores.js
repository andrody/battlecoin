Viewer = new Mongo.Collection('viewer');

//Viewer._ensureIndex({createdAt: 1}, {expireAfterSeconds: 70});

// Entidades expiram em X segundos
//Viewer._ensureIndex( { "createdAt": 1 }, { expireAfterSeconds: 70 } );

// Guarda id do ultimo canal acessado antes de uma sessao ser perdida
//LastSessionLosted = new Mongo.Collection('lastSessionLosted');
//LastSessionLosted.remove({});

// NAO SENDO USADO
Viewer.createViewer = function(mySessionId, channel_id){

	var viw = Viewer.findOne({sessionId: mySessionId});
	var username = Session.get("channel_username");
	var channel = Channels.findOne({username: username});

	console.log("Chamou createViewer");
	console.log(username);
	console.log(channel);
	console.log(mySessionId);
	console.log(Meteor.connection._lastSessionId);
	console.log(Meteor.default_connection._lastSessionId);
	//console.log(channel._id);

	if(viw) {
		Viewer.update(viw._id, {$set: {channel_id: channel._id, createdAt: new Date()}} );
	} else {
		Viewer.insert({sessionId: mySessionId, channel_id: channel._id, createdAt: new Date() });
	}

	var count = Viewer.find({channel_id: channel._id}).count();

	console.log("Espectadores de " + channel._id + " = " + count);
	console.log(Session.get("channel_username"));

	Channels.update(channel._id, {$set: {viewers: count} } );
};

