// Rotas

/*
ClientTimer = function(){

  var path;
  var zeitDep = new Deps.Dependency(); // !!!
  var zeitValue;
  var zeitInterval;

  function timerFunc() {
    var zeit = new Date();
    
    var mySessionId = Meteor.default_connection._lastSessionId;

    if(mySessionId) {

        console.log("My session id: " + mySessionId);

        var viewerSession = Viewer.findOne({sessionId: mySessionId});

        if(!viewerSession) {
            console.log("Adicionando espectador!. " + path);
            Viewer.insert({sessionId: mySessionId, path: path});
        } else {
            Meteor.call("updateViewer", mySessionId, path);
        }
        
        Destro1yed();
    }

    zeitDep.changed(); // !!!
  };

  StartVerify = function() {
    timerFunc(); // Call it once so that we'll have an initial value 
    zeitInterval = Meteor.setInterval(timerFunc, 2000);
  };

  Destro1yed = function() {
    console.log("Destro1yed");
    Meteor.clearInterval(zeitInterval);
  };

  this.Start = function(caminho) {
        path = caminho;
        StartVerify();
  };
}

var verifier = new ClientTimer();
*/

Router.configure({
    layoutTemplate: "sidebar"
});

Router.configure({
    layoutTemplate: "home"
});

Router.configure({
    layoutTemplate: "streamTemplate"
});

Router.configure({
    layoutTemplate: "ForgotPassword"
});

Router.configure({
    layoutTemplate: "ResetPassword"
});

Router.configure({
    layoutTemplate: "embed"
});

Router.route("/", function() {
    this.render("main");
}, {
    name: "main",
    template: "main",
    layoutTemplate: "sidebar"

});

Router.route("/channel/:username", {
    name: 'channel',
    template: 'channel',
    layoutTemplate: "sidebar",

    data: function() {
        var channelName = this.params.username;

        Session.set("channelSelected", channelName);
        Session.set("fullscream", 'partial');
        //console.log(findOne({ _id: currentList }));

        return Channels.findOne({
            username: channelName
        });
    },

});

/*
Router.route("/channel/:_id", {
  name: 'channel',
  template: 'channel',
    layoutTemplate: "sidebar",

  data: function(){
        var channelId = this.params._id;

        Session.set("channelId", channelId);
        //console.log(findOne({ _id: currentList }));

        return Channels.findOne({ name: channelId });
    },

});
*/

Router.route("/streamTemplate", {
    name: 'streamTemplate',
    template: 'streamTemplate',
    layoutTemplate: "streamTemplate",

    onRun: function() {
        console.log("ACESSEI UM CANAL!.");

        this.next();
    }
});

Router.route("/embed/:username", {
    name: 'embed',
    template: 'embed',
    layoutTemplate: "embed",

    data: function() {
        var username = this.params.username;

        if (username) {

            Session.set("channelSelected", username);

            Session.set("fullscream", 'total');

            Session.set("channel_username", username);

            return Channels.findOne({
                username: username
            });
        }

        //var mySession = Meteor.default_connection._lastSessionId;
        //Meteor.call("insertViewer", mySession, username);

        //var timer = new EmbedTimer();
        //timer.Start();

        //var verifier = new ClientTimer();
        //verifier.Start();

        //Session.set("channelId", channelId);
        //console.log(findOne({ _id: currentList }));

    },
    onBeforeAction: function() {
        var username = this.params.username;

        if (username) {
            Session.set("channel_username", username);
        }
    }

});

Router.route("/ForgotPassword", {
    name: 'ForgotPassword',
    template: 'ForgotPassword',
    layoutTemplate: "ForgotPassword",
});

Router.route("/ResetPassword", {
    name: 'ResetPassword',
    template: 'ResetPassword',
    layoutTemplate: "ResetPassword",
});

Router.route("navigation");
Router.route("page1");
//Router.route("signUp2");
Router.route("sidebar");
//Router.route("login");
//Router.route("signUp");
Router.route("browse");


/* Rota para stream do usuário
 * Aqui eu procuro a stream online no momento com o HttpClient
 * twittv / ustream

 * Instancia um httpClient a partir do nome usuário battecloin recebido no parametro
 * com esse nome de usuário tenho acesso aos nicknames nos outros serviços
 * salvos no banco de dados.
 */
Router.route("stream/:user", {
    name: "stream",
    template: "stream",

    data: function() {
        var user = this.params.user;
        var src = "http://www.twitch.tv/" + user + "/embed";
        console.log("User: " + user);

        return {
            user: user,
            src: src
        };
    }
});

Router.route("teste");
